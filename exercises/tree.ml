(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-34-37-39"]

open Fold
open Mu

module CTree = struct
  type 'a f = NYI

  let map _ _ = failwith "NYI"
end

module Tree = Mu (CTree)

let leaf () = failwith "NYI"

let node l a r = failwith "NYI"

let rec from_tree _ = failwith "NYI"
