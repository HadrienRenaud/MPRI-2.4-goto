(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-33-34-37-39"]

open Fold
open Rose

(*****************************************************************)
(* Algebra                                                       *)
(*****************************************************************)

module AlgFlatten = struct
  module F = CRose

  type carrier1 = NYI1

  type carrier2 = NYI2

  let alg1 _flattena _flattens = failwith "NYI"

  let alg2 _flattena _flattens = failwith "NYI"
end

(*****************************************************************)
(* Fixpoint                                                      *)
(*****************************************************************)

let flattena _ = failwith "NYI"

let flattens _ = failwith "NYI"

(*****************************************************************)
(* Tests                                                         *)
(*****************************************************************)

let%test _ =
  let x = Spec.RNode (0, Spec.RNil) in
  Spec.flattena x = flattena (from_rose x)

let%test _ =
  let x = Spec.RNode (0, Spec.RCons (Spec.RNode (3, Spec.RNil), Spec.RNil)) in
  Spec.flattena x = flattena (from_rose x)

let%test _ =
  let x =
    Spec.RNode
      ( 0
      , Spec.RCons
          ( Spec.RNode (3, Spec.RNil)
          , Spec.RCons (Spec.RNode (2, Spec.RNil), Spec.RNil) ) )
  in
  Spec.flattena x = flattena (from_rose x)

let%test _ =
  let x =
    Spec.RNode
      ( 0
      , Spec.RCons
          ( Spec.RNode (3, Spec.RCons (Spec.RNode (1, Spec.RNil), Spec.RNil))
          , Spec.RCons (Spec.RNode (2, Spec.RNil), Spec.RNil) ) )
  in
  Spec.flattena x = flattena (from_rose x)
