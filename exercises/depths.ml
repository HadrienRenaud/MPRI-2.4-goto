(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-20-27-32-33-34-37-39"]

open Fold
open Tree

(*****************************************************************)
(* Algebra                                                       *)
(*****************************************************************)

module AlgDepths = struct
  module F = CTree

  type carrier = NYI

  let alg _total = failwith "NYI"
end

(*****************************************************************)
(* Fixpoint                                                      *)
(*****************************************************************)

let depths _ = failwith "NYI"

(*****************************************************************)
(* Tests                                                         *)
(*****************************************************************)

let%test _ =
  let x = Spec.Leaf in
  from_tree (Spec.depths x 0) = depths (from_tree x) 0

let%test _ =
  let x = Spec.Node (Spec.Leaf, 1, Spec.Leaf) in
  from_tree (Spec.depths x 0) = depths (from_tree x) 0

let%test _ =
  let x = Spec.Node (Spec.Node (Spec.Leaf, 2, Spec.Leaf), 1, Spec.Leaf) in
  from_tree (Spec.depths x 0) = depths (from_tree x) 0

let%test _ =
  let x = Spec.Node (Spec.Leaf, 1, Spec.Node (Spec.Leaf, 2, Spec.Leaf)) in
  from_tree (Spec.depths x 0) = depths (from_tree x) 0
