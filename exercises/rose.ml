(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-34-37-39"]

open Fold
open Mu

module CRose = struct
  type ('a, 'b) f1 = NYI

  type ('a, 'b) f2 = NYI

  let map1 _ _ = failwith "NYI"

  let map2 _ _ = failwith "NYI"
end

module Rose = Mu2 (CRose)

let node n ts = failwith "NYI"

let nil () = failwith "NYI"

let cons t ts = failwith "NYI"

let rec from_rose _ = failwith "NYI"

and from_roses _ = failwith "NYI"
