(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-33-39"]

open Algebra
open Mu

(*****************************************************************)
(* Generic recursor                                              *)
(*****************************************************************)

module Fix (A : AlgCurryFix) = struct
  module MuF = Mu (A.F)
  module MuG = Mu (A.G)

  let rec fix _ = failwith "NYI"
end

(* Section 5.4 *)
