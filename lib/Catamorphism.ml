(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-33-39"]

open Algebra
open Mu

(*****************************************************************)
(* Generic recursor                                              *)
(*****************************************************************)

module Fix (A : AlgCata) = struct
  module MuF = Mu (A.F)
  open MuF

  let rec fix _ = failwith "NYI"
end
